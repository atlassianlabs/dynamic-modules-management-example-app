### Dynamic Modules Management example app

This is an example app which shows how to manage dynamic modules.

**Running**

`mvn spring-boot:run -Drun.arguments="--addon.base-url=<your-url-here>"`

`ngrok http 3000 -hostname=<your-url-here>`

**Short overview**

This application is made of two components:

* Spring Boot Backend
   * was created with use of [atlassian-connect-spring-boot](https://bitbucket.org/atlassian/atlassian-connect-spring-boot/src/master/)
   * is responsible for communication with Atlassian services and serving the frontend.
   * key part of code is available in `com/atlassian/connect/jira/DynamicModulesManagementController.java`

* React Frontend

**Folders structure**

* `src/main/java` - Java backend source code
* `src/main/js` - JavaScript frontend source code
* `src/main/resources/static` - Files that are statically served by Spring Boot

**Build process**

1. Maven installs node and yarn
1. Frontend dependencies are installed
1. Frontend is built, taking source code from `src/main/js` directory, the compiled bundle is put into `target/classes/static`
1. Spring Boot app is built

**DevLoop**

`yarn start`

**Screens**

* Adding editable field
![Alt text](screens/Adding_editable_field.png?raw=true "Adding editable fields")

Underlying request:
```
{
  "jiraIssueFields": [
    {
      "key": "number-of-required-qa-approvals",
      "type": "number",
      "name": {
        "value": "Number of required QA's approvals"
      },
      "description": {
        "value": "This value shows te required number of approvals required to merge this change."
      }
    }
  ]
}
```
 
* Adding read-only field
![Alt text](screens/Adding_read_only_field.png?raw=true "Adding editable fields")

Underlying request:
```
{
  "jiraIssueFields": [
    {
      "key": "team-slogan",
      "type": "read_only",
      "name": {
        "value": "Number of required QA's approvals"
      },
      "description": {
        "value": "Totally unuseful data which can be stored here."
      },
      "property": {
        "path": "path.in.object",
        "key": "team-slogan",
        "type": "string"
      }
    }
  ]
}
```

* Error reporting
![Alt text](screens/Error_handling.png?raw=true "Adding editable fields")

**License**
This project is licensed under the [Apache License, Version 2.0](LICENCE.txt).