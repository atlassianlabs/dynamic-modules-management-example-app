package com.atlassian.connect.jira;

import com.atlassian.connect.jira.beans.JiraIssueFieldsRequestBean;
import com.atlassian.connect.jira.exceptions.ModuleRegistrationException;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.cors.CorsConfiguration;

@Controller
@CrossOrigin(origins = CorsConfiguration.ALL)
public class DynamicModulesManagementController {

    private static final String DYNAMIC_MODULES_URI = "/rest/atlassian-connect/1/app/module/dynamic";

    @Autowired
    private AtlassianHostRestClients atlassianHostRestClients;

    @RequestMapping("/dynamicModulesManagement")
    @IgnoreJwt
    public String dynamicModulesManager() {
        return "dynamicModules";

    }

    @RequestMapping(value = "/register-dynamic-module", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public JiraIssueFieldsRequestBean registerDynamicModule(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody JiraIssueFieldsRequestBean request) throws ModuleRegistrationException {
        JiraIssueFieldsRequestBean response;
        try {
            response = atlassianHostRestClients.authenticatedAsAddon()
                    .exchange(
                            hostUser.getHost().getBaseUrl() + DYNAMIC_MODULES_URI,
                            HttpMethod.POST,
                            new HttpEntity<>(request, baseHeaders()),
                            JiraIssueFieldsRequestBean.class
                    ).getBody();
        } catch (HttpClientErrorException e) {
            throw new ModuleRegistrationException(e.getResponseBodyAsString());
        }
        return response;
    }

    @RequestMapping(value = "/list-dynamic-modules", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public JiraIssueFieldsRequestBean listMyRegisteredDynamicModules(@AuthenticationPrincipal AtlassianHostUser hostUser) {
        return atlassianHostRestClients.authenticatedAsAddon()
                .exchange(
                        hostUser.getHost().getBaseUrl() + DYNAMIC_MODULES_URI,
                        HttpMethod.GET,
                        new HttpEntity<>("", baseHeaders()),
                        JiraIssueFieldsRequestBean.class
                ).getBody();
    }

    @RequestMapping(value = "/delete-dynamic-module", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public String deleteDynamicModule(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestParam(value = "moduleKey") String moduleKey) {
        try {
            return atlassianHostRestClients.authenticatedAsAddon()
                    .exchange(
                            hostUser.getHost().getBaseUrl() + DYNAMIC_MODULES_URI + "?moduleKey=" + moduleKey,
                            HttpMethod.DELETE,
                            new HttpEntity<>("", baseHeaders()),
                            new ParameterizedTypeReference<String>() {
                            }
                    ).getBody();
        } catch (HttpClientErrorException e) {
            return e.getStatusText();
        }
    }

    private LinkedMultiValueMap<String, String> baseHeaders() {
        LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Accept", "application/json");
        return headers;
    }

    @ExceptionHandler({ModuleRegistrationException.class})
    public ResponseEntity<String> handleException(ModuleRegistrationException ex) {
        System.out.println(ex.getMessage());
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
