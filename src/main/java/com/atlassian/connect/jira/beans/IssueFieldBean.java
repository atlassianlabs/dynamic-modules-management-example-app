package com.atlassian.connect.jira.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

@JsonAutoDetect
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IssueFieldBean {
    private final String key;
    private final String type;
    private final Map<String, String> name;
    private final Map<String, String> description;
    private final Map<String, String> property;

// The code below has been generated by BoB the Builder of Beans based on the class' fields.
// Everything after this comment will be regenerated if you invoke BoB again.
// If you don't know who BoB is, you can find him here: https://bitbucket.org/atlassianlabs/bob-the-builder-of-beans

    @JsonCreator
    public IssueFieldBean(@JsonProperty("key") String key, @JsonProperty("type") String type, @JsonProperty("name") Map<String, String> name, @JsonProperty("description") Map<String, String> description, @JsonProperty("property") Map<String, String> property) {
        this.key = key;
        this.type = type;
        this.name = name != null ? new HashMap<>(name) : null;
        this.description = description != null ? new HashMap<>(description) : null;
        this.property = property != null ? new HashMap<>(property) : null;
    }

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public Map<String, String> getName() {
        return name;
    }

    public Map<String, String> getDescription() {
        return description;
    }

    public Map<String, String> getProperty() {
        return property;
    }

    public static IssueFieldBean.Builder builder() {
        return new IssueFieldBean.Builder();
    }

    public static IssueFieldBean.Builder builder(IssueFieldBean data) {
        return new IssueFieldBean.Builder(data);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IssueFieldBean that = (IssueFieldBean) o;

        return Objects.equals(this.getKey(), that.getKey()) && Objects.equals(this.getType(), that.getType()) && Objects.equals(this.getName(), that.getName()) && Objects.equals(this.getDescription(), that.getDescription()) && Objects.equals(this.getProperty(), that.getProperty());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getKey(), getType(), getName(), getDescription(), getProperty());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", this.getClass().getSimpleName() + "[", "]")
                .add("key=" + getKey())
                .add("type=" + getType())
                .add("name=" + getName())
                .add("description=" + getDescription())
                .add("property=" + getProperty())
                .toString();
    }

    public static final class Builder {

        private String key;
        private String type;
        private Map<String, String> name = new HashMap<>();
        private Map<String, String> description = new HashMap<>();
        private Map<String, String> property = new HashMap<>();

        private Builder() {
        }

        private Builder(IssueFieldBean initialData) {
            this.key = initialData.getKey();
            this.type = initialData.getType();
            this.name = new HashMap<>(initialData.getName());
            this.description = new HashMap<>(initialData.getDescription());
            this.property = new HashMap<>(initialData.getProperty());
        }

        public Builder setKey(String key) {
            this.key = key;
            return this;
        }

        public Builder setType(String type) {
            this.type = type;
            return this;
        }

        public Builder setName(Map<String, String> name) {
            this.name = name;
            return this;
        }

        public Builder addName(Map<String, String> name) {
            this.name.putAll(name);
            return this;
        }

        public Builder setDescription(Map<String, String> description) {
            this.description = description;
            return this;
        }

        public Builder addDescription(Map<String, String> description) {
            this.description.putAll(description);
            return this;
        }

        public Builder setProperty(Map<String, String> property) {
            this.property = property;
            return this;
        }

        public Builder addProperty(Map<String, String> property) {
            this.property.putAll(property);
            return this;
        }

        public IssueFieldBean build() {
            return new IssueFieldBean(key, type, name, description, property);
        }
    }
}
