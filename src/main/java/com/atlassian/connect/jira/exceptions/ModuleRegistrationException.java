package com.atlassian.connect.jira.exceptions;

public class ModuleRegistrationException extends Throwable {

    public ModuleRegistrationException(String message) {
        super(message);
    }
}
