import React from 'react'
import Button from '@atlaskit/button';

const DynamicModulesTable = ({jiraIssueFields, deleteDynamicModule, rerender, popup}) => (
        <div className="dynamic-modules-table">
            <h3>Registered dynamic modules:</h3>
            <table>
                <thead>
                <tr>
                    <th>Key</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Property</th>
                    <th className="description-column">Description</th>
                    <th className="button-column">Action</th>
                </tr>
                </thead>
                <tbody>
                {jiraIssueFields && jiraIssueFields.map((dynamicModule) => {
                    let property = null;
                    if (dynamicModule.property != null) {
                        property = <>
                            path: {dynamicModule.property.path} <br/>
                            key: {dynamicModule.property.key} <br/>
                            type: {dynamicModule.property.type}<br/>
                        </>;

                    }
                    return (
                    <tr key={dynamicModule.key}>
                        <td>{dynamicModule.key}</td>
                        <td>{dynamicModule.type}</td>
                        <td>{dynamicModule.name.value}</td>
                        <td>{property}</td>
                        <td>{dynamicModule.description.value}</td>
                        <td><Button appearance='danger'
                                    onClick={() => deleteDynamicModule(dynamicModule.key).then(rerender).catch(popup)}>Delete</Button>
                        </td>
                    </tr>
                    )
                })}
                </tbody>
            </table>
        </div>
);

export default DynamicModulesTable