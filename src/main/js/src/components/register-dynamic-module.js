import React from 'react'
import Form, {Field} from '@atlaskit/form';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/textfield';
import {Grid, GridColumn} from '@atlaskit/page';

const RegisterDynamicModule = ({onSubmit}) => (
    <div>
        <h3>Register a new module (issue field):</h3>
        <Form onSubmit={data => onSubmit(data)}>
            {({formProps}) => (
                <form {...formProps}>
                    <Grid className="register-dynamic-module-form" layout="fluid">
                        <GridColumn medium={2}>
                            <Field name="key" defaultValue="" label="Key" isRequired>
                                {({fieldProps}) => <TextField {...fieldProps} />}
                            </Field>
                        </GridColumn>
                        <GridColumn medium={2}>
                            <Field name="type" defaultValue="" label="Type" isRequired>
                                {({fieldProps}) => <TextField {...fieldProps} />}
                            </Field>
                        </GridColumn>
                        <GridColumn medium={2}>
                            <Field name="readOnlyType" defaultValue="" label="Read-only type">
                                {({fieldProps}) => <TextField {...fieldProps} />}
                            </Field>
                        </GridColumn>
                        <GridColumn medium={3}>
                            <Field name="name" defaultValue="" label="Name" isRequired>
                                {({fieldProps}) => <TextField {...fieldProps} />}
                            </Field>
                        </GridColumn>
                        <GridColumn medium={3}>
                            <Field name="description" defaultValue="" label="Description" isRequired>
                                {({fieldProps}) => <TextField {...fieldProps} />}
                            </Field>
                        </GridColumn>
                    </Grid>
                    <br/>
                    <Button className="button-submit" type="submit" appearance="primary">
                        Submit
                    </Button>
                </form>
            )}
        </Form>
    </div>
);

export default RegisterDynamicModule