function getToken() {
    return new Promise((resolve, reject) => {
        window.AP.context.getToken(function (token) {
                resolve(token);
            });
        }
    )
}

function registerDynamicModule(data) {
    return new Promise((resolve, reject) => {
            return getToken()
                .then((token) => {
                    let requestBody = {};
                    let jiraIssueFields = [];
                    let issueField = {
                        key: data.key.trim(),
                        type: data.type.trim(),
                        name: {
                            value: data.name.trim()
                        },
                        description: {
                            value: data.description.trim()
                        },
                    };
                    requestBody.jiraIssueFields = jiraIssueFields;
                    requestBody.jiraIssueFields.push(issueField);

                    if (data.type.trim().toLowerCase() === "read_only") {
                        requestBody.jiraIssueFields[0].property = {
                            path: 'path.in.object',
                            key: data.key.trim(),
                            type: data.readOnlyType.trim()
                        };
                    }
                    return fetch(
                        '/register-dynamic-module',
                        {
                            method: 'POST',
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                'Authorization': 'JWT ' + token,
                            },
                            body: JSON.stringify(requestBody)
                        }
                    )
                        .then(response => {
                            if (response.ok) {
                                resolve();
                            } else {
                                response.json().then(res => {
                                    reject(res.message)
                                })
                            }
                        })
                        .catch(response => console.log("Error during registering.", response.json()))
                })
        }
    )
}

function getAllDynamicModules() {
    return getToken()
        .then((token) => {
            return fetch(
                '/list-dynamic-modules',
                {
                    method: 'GET',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'JWT ' + token,
                    }
                }
            )
                .then((value) => value.json())
        })
}

function deleteDynamicModule(moduleKey) {
    return new Promise((resolve, reject) => {
        return getToken()
            .then((token) => {
                return fetch(
                    '/delete-dynamic-module?moduleKey='.concat(moduleKey),
                    {
                        method: 'DELETE',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                            'Authorization': 'JWT ' + token,
                        }
                    }
                )
                    .then(response => {
                        if (response.ok) {
                            resolve();
                        } else {
                            response.json().then(res => {
                                reject(res.message)
                            })
                        }
                    })
                    .catch(response => console.log("Error during deleting module.", response.json()))
            })
    })
}

const dynamicModuleService = {
    registerDynamicModule: registerDynamicModule,
    getAllDynamicModules: getAllDynamicModules,
    deleteDynamicModule: deleteDynamicModule,
};

module.exports = dynamicModuleService;