import React from 'react';
import ReactDOM from 'react-dom';
import '@atlaskit/css-reset';
import Page from '@atlaskit/page';
import { AutoDismissFlag, FlagGroup } from '@atlaskit/flag';
import dynamicModulesService from './dynamic-modules-service';
import DynamicModulesTable from './components/dynamic-modules-table';
import RegisterDynamicModule from './components/register-dynamic-module';
import './index.css';

const MAX_POPUP_LENGTH = 250;

const registerDynamicModule = dynamicModulesService.registerDynamicModule;
const getAllDynamicModules = dynamicModulesService.getAllDynamicModules;
const deleteDynamicModule = dynamicModulesService.deleteDynamicModule;

class DynamicModulesManagement extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            dynamicModules: [],
            flags: [],
        };
    }

    componentDidMount() {
        this.fetchDynamicModules()
    }

    fetchDynamicModules() {
        getAllDynamicModules()
            .then((res) => {
                this.setState({dynamicModules: res})
            })
            .catch(() => {
                console.log("Problem with loading dynamic modules has occurred.")
            })
    }

    popup(message) {
        if (message.length > MAX_POPUP_LENGTH) {
            console.log(message);
            message = message.slice(0, MAX_POPUP_LENGTH)
        }
        this.setState({flags: [...this.state.flags, message]})
    }

    dismissFlag() {
        this.setState(state => ({flags: state.flags.slice(1)}));
    }

    onSubmit(data) {
        data && registerDynamicModule(data)
            .then(() => this.fetchDynamicModules())
            .catch(message => this.popup(message))
    }

    render() {
        return (
            <Page>
                <h2>Dynamic modules management</h2>
                <RegisterDynamicModule onSubmit={(data) => this.onSubmit(data)}/>
                <DynamicModulesTable jiraIssueFields={this.state.dynamicModules.jiraIssueFields}
                                     deleteDynamicModule={deleteDynamicModule}
                                     rerender={() => this.fetchDynamicModules()}
                                     popup={(message) => this.popup(message)}/>
                <FlagGroup onDismissed={(()=>this.dismissFlag())}>
                    {this.state.flags.map(flag => (
                        <AutoDismissFlag description={flag} key={flag} id={flag}/>
                    ))}
                </FlagGroup>
            </Page>
        )
    }
}

ReactDOM.render(<DynamicModulesManagement/>, document.getElementById('root'));
